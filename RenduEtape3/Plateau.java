package composants;


/**
 * Cette classe permet de gérer un plateau de jeu constitué d'une grille de pièces (grille de 7 lignes sur 7 colonnes).
 *
 */
public class Plateau {

	private Piece plateau[][]; // La grille des pièces.

	/**
	 * A Faire (25 avril sylvain terminer)
	 * 
	 * Constructeur permettant de construire un plateau vide (sans pièces) et d'une taille de 7 lignes sur 7 colonnes.
	 */
	public Plateau() {
		this.plateau=new Piece[7][7];
	}

	/**
	 * A Faire (25 avril sylvain terminer)
	 * 
	 * Méthode permettant de placer une pièce sur le plateau.
	 * 
	 * @param piece La pièce à placer.
	 * @param lignePlateau La ligne du plateau sur laquelle sera placée la pièce (un entier entre 0 et 6).
	 * @param colonnePlateau La colonne du plateau sur laquelle sera placée la pièce (une entier entre 0 et 6).
	 */
	public void positionnePiece(Piece piece,int lignePlateau,int colonnePlateau){
		this.plateau[lignePlateau][colonnePlateau]=piece;
	}

	/**
	 *A Faire (25 avril sylvain terminer)
	 * 
	 * Méthode retournant une pièce se trouvant sur le plateau à un emplacement spécifique.
	 * 
	 * @param lignePlateau La ligne du plateau  (un entier entre 0 et 6).
	 * @param colonnePlateau La colonne du plateau (un entier entre 0 et 6).
	 * @return La pièce se trouvant sur la ligne lignePlateau et la colonne colonnePlateau. Si il n'y a pas de pièce, null est retourné.
	 */
	public Piece getPiece(int lignePlateau,int colonnePlateau){
		return this.plateau[lignePlateau][colonnePlateau];
	}

	/**
	 * 
	 * A Faire (26 avril Sylvain à tester)
	 *  
	 * Méthode permettant de placer aléatoirment 49 pièces du jeu sur le plateau.
	 * L'orientation des pièces est aléatoire. Les pièces utilisées doivent être des nouvelles pièces générées à partir de la méthode Piece.nouvellesPieces.
	 * Parmi les 50 pièces du jeu, la pièce qui n'a pas été placée sur le plateau est retournée par la méthode.
	 * 
	 * @return La seule pièce qui n'a pas été placée sur le plateau
	 */
	public Piece placerPiecesAleatoierment(){
		Piece[] tab = Piece.nouvellesPieces();
		int numPiece=0;
		for(int ligne=0;ligne<this.plateau.length;ligne++) {
			for(int colonne=0;colonne<this.plateau.length;colonne++) {
				int endroit = Utils.genererEntier(49-numPiece);
				Piece pieceChoisi=tab[endroit];
				this.plateau[ligne][colonne]=pieceChoisi;
				pieceChoisi=null;
				for(int deplacement=endroit;deplacement<tab.length-1;deplacement++) {
					Piece tmp = tab[endroit];
					tab[endroit]=tab[endroit+1];
					tab[endroit+1]=tmp;
					
				}
				numPiece++;
			}
		}
		return tab[tab.length-1]; 
	}

	/**
	 * 
	 * Méthode utilitaire permettant de tester si les positions passées en paramètre sont les positions de deux cases différentes et adjacentes d'une grille de 7 lignes sur 7 colonnes.
	 *  
	 * @param posLigCase1 Un entier quelconque.
	 * @param posColCase1 Un entier quelconque.
	 * @param posLigCase2 Un entier quelconque.
	 * @param posColCase2 Un entier quelconque.
	 * @return true si les les positions passées en paramètre sont les positions de deux cases différentes et adjacentes d'une grille de 7 lignes sur 7 colonnes, false sinon.
	 */
	private static boolean casesAdjacentes(int posLigCase1,int posColCase1,int posLigCase2,int posColCase2){
		if ((posLigCase1<0)||(posLigCase2<0)||(posLigCase1>6)||(posLigCase2>6)) return false;
		if ((posColCase1<0)||(posColCase2<0)||(posColCase1>6)||(posColCase2>6)) return false;
		int distLigne=posLigCase1-posLigCase2;
		if (distLigne<0) distLigne=-distLigne;
		int distColonne=posColCase1-posColCase2;
		if (distColonne<0) distColonne=-distColonne;
		if ((distLigne>1)||(distColonne>1)||((distColonne+distLigne)!=1))
			return false;
		return true;
	}

	/**
	 * 
	 * A Faire (26 avril sylvain à tester)
	 * 
	 * Méthode permettant de tester si les positions passées en paramètre sont les positions de deux cases différentes et adjacentes 
	 * de la grille de jeu et qu'il est possible de passer d'une cas à l'autre compte tenu des deux pièces posées sur les deux cases du plateau.
	 * 
	 * @param posLigCase1 Un entier quelconque.
	 * @param posColCase1 Un entier quelconque.
	 * @param posLigCase2 Un entier quelconque.
	 * @param posColCase2 Un entier quelconque.
	 * @return true si les positions passées en paramètre sont les positions de deux cases différentes et adjacentes de la grille de jeu et qu'il est possible de passer d'une cas à l'autre compte tenu des deux pièces posées sur les deux cases du plateau, false sinon.
	 */
	private boolean passageEntreCases(int posLigCase1,int posColCase1,int posLigCase2,int posColCase2){
		// remettre private 
		Piece caseDeDepart=this.plateau[posLigCase1][posColCase1];
		Piece caseArrivee=this.plateau[posLigCase2][posColCase2];
		
		if(casesAdjacentes(posLigCase1,posColCase1,posLigCase2,posColCase2)) {
			if(posLigCase2==posLigCase1+1) {
				if((caseDeDepart.getPointEntree(2)==caseArrivee.getPointEntree(0)) && (caseDeDepart.getPointEntree(2)==true)) {
					return true;
				}
				else {
					return false;
				}
			}
			else if(posLigCase2==posLigCase1-1) {
				if((caseDeDepart.getPointEntree(0)==caseArrivee.getPointEntree(2)) && (caseDeDepart.getPointEntree(0)==true)) {
					return true;
				}
				else {
					return false;
				}
			}
			else if(posColCase2==posColCase1+1) {
				if((caseDeDepart.getPointEntree(1)==caseArrivee.getPointEntree(3)) && (caseDeDepart.getPointEntree(1)==true)) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				if((caseDeDepart.getPointEntree(3)==caseArrivee.getPointEntree(1)) && (caseDeDepart.getPointEntree(3)==true)) {
					return true;
				}
				else {
					return false;
				}
			}
		
		}
		else {
			return false;
		}
		
	}
	
	public static int[][] caseVoisine(int ligne, int colonne,int ligneAncienne, int colonneAncienne){
		int [][] tab = new int[4][2];
		
		int [][] tabFinal = new int[4][2];
		int avancementTab=0;
		
		tab[0][0]=ligne+1;
		tab[0][1]=colonne;
		
		tab[1][0]=ligne;
		tab[1][1]=colonne+1;
		
	
		tab[2][0]=ligne-1;
		tab[2][1]=colonne;
		
		tab[3][0]=ligne;
		tab[3][1]=colonne-1;
		
		for(int i=0;i<tab.length;i++) {
			if(((tab[i][0]<7 && tab[i][0]>=0) && (tab[i][1]<7 && tab[i][1]>=0))) {
				tabFinal[avancementTab]=tab[i];
				avancementTab++;
			}
		}
		
		for(int j=avancementTab;j<tabFinal.length;j++) {
			tabFinal[j][0]=-1;
			tabFinal[j][1]=-1;

		}
		
		for(int i=0;i<tabFinal.length;i++) {
			if((tabFinal[i][0]==ligneAncienne) && (tabFinal[i][1]==colonneAncienne)) {
				tabFinal[i][0]=-1;
				tabFinal[i][1]=-1;
			}
		}
	
		
		
		return tabFinal;
	}
	 
	/**
	 * 
	 * Sylvain 7 mai Ok
	 * 
	 * Méthode permettant de retourner un éventuel chemin entre deux cases du plateau compte tenu des pièces posées sur le plateau.
	 * Dans le cas où il n'y a pas de chemin entre les deux cases, la valeur null est retournée.
	 * Dans le cas où il existe un chemin, un chemin possible est retourné sous forme d'un tableau d'entiers à deux dimensions.
	 * La première dimension correspond aux cases du plateau à emprunter pour aller de la case de départ à la case d'arrivée.
	 * Dans ce tableau, chaque case est un tableau de deux entiers avec le premier entier qui correspond à la ligne de la case et
	 * le second entier qui correspond à la colonne de la case. La première case d'un chemin retourné correspond toujours 
	 * à la case (posLigCaseDep,posColCaseDep) et la dernière case correspond toujours à la case (posLigCaseArr,posColCaseArr).
	 *
	 * @param posLigCaseDep La ligne de la case de départ (un entier compris entre 0 et 6).
	 * @param posColCaseDep La colonne de la case de départ (un entier compris entre 0 et 6).
	 * @param posLigCaseArr La ligne de la case d'arrivée (un entier compris entre 0 et 6).
	 * @param posColCaseArr La colonne de la case d'arrivée (un entier compris entre 0 et 6).
	 * @return null si il n'existe pas de chemin entre les deux case, un chemin sinon.
	 */
	public int[][] calculeChemin(int posLigCaseDep,int posColCaseDep,int posLigCaseArr,int posColCaseArr){
		
		if((posLigCaseDep==posLigCaseArr) && (posColCaseDep==posColCaseArr)) {
			return null;
		} // si les 2 cases sont identiques
		
		int[][] listeChemin = new int[49][49*2];
		int[][] caseDeDepart = new int[50][2];
		int[] voisinTrouve = new int[98];
		int posCheminFinal=0;
		int[] caseVisite = new int[49*2];
		
		for(int i=0;i<caseVisite.length;i++) {
			caseVisite[i]=-1;
		}
		
		for(int initI=0;initI<listeChemin.length;initI++) {
			for(int initJ=0;initJ<listeChemin[0].length;initJ++) {
				// on met à 0 tout les element de liste chemin
				listeChemin[initI][initJ]=-1;
			}
		}
		
		listeChemin[0][0]=posLigCaseDep;
		listeChemin[0][1]=posColCaseDep;
		
		
		for(int i=0;i<caseDeDepart.length;i++) {
			for(int j=0;j<caseDeDepart[0].length;j++) {
				caseDeDepart[i][j]=-1;
			}
		} // on lui place tout les attributs de case de depart a -1;
		
		for(int i=0;i<voisinTrouve.length;i++) {
			voisinTrouve[i]=-1;
		}
		
		caseDeDepart[0][0]=posLigCaseDep;
		caseDeDepart[0][1]=posColCaseDep; // on insere dans le tab caseDepart la position de base
		
		boolean fin = false;
		
		int[][] tabTempo = new int[caseDeDepart.length][caseDeDepart[0].length]; // on crée une copie de caseDeDepart
		int ligneAncienneCase=caseDeDepart[0][0];
		int colonneAncienneCase=caseDeDepart[0][1];
		int[][] listeCheminCopy = new int[listeChemin.length][listeChemin[0].length];
		
		while ((fin==false)){
			// tant qu'il n'a rien trouvé
			
			for(int i=0;i<listeCheminCopy.length;i++) { 
				for(int j=0;j<listeCheminCopy[0].length;j++) {
					listeCheminCopy[i][j]=-1;
				}
			}
			
			// on copie les element de listeChemin dans la copie et on met tt les elments de listeChemin à -1
			
			for(int i=0;i<listeChemin.length;i++) {
				for(int k=0;k<listeChemin[0].length;k++) {
					listeCheminCopy[i][k]=listeChemin[i][k];
					listeChemin[i][k]=-1;
				}
			}
		
			
			for(int i=0;i<tabTempo.length;i++) { 
				for(int j=0;j<tabTempo[0].length;j++) {
					tabTempo[i][j]=-1;
				}
			}

			for(int caseBase=0;caseBase<caseDeDepart.length && fin==false;caseBase++) {

				int numLigneCaseDepart=0;
				int numColonneCaseDepart=0;
				numLigneCaseDepart = caseDeDepart[caseBase][0];
				numColonneCaseDepart = caseDeDepart[caseBase][1];
				
				int placeV=0;
				boolean ok=true;
				while(placeV<caseVisite.length-1 && ok) {
					if(caseVisite[placeV]==-1) {
						caseVisite[placeV]=numLigneCaseDepart;
						caseVisite[placeV+1]=numColonneCaseDepart;
						ok=false;
					}
					else {
						placeV++;
					}
				}
				
				if(numLigneCaseDepart!=-1) {
					int [][] voisin=caseVoisine(numLigneCaseDepart,numColonneCaseDepart,ligneAncienneCase,colonneAncienneCase);
					for(int k=0;k<voisin.length && fin==false;k++) {
						if((voisin[k][0] !=-1)) {
							if(passageEntreCases(numLigneCaseDepart,numColonneCaseDepart,voisin[k][0],voisin[k][1])) {
								int place=0;
								while((tabTempo[place][0]!=-1) && (place<tabTempo.length -1)) {
									place++;
								}
								
								boolean possible=true;
								for(int i=0;i<caseVisite.length;i=i+2) {
									if((caseVisite[i]==voisin[k][0]) && (caseVisite[i+1]==voisin[k][1])) {
										possible=false;
									}
								}
								
								if(possible){
									tabTempo[place][0]=voisin[k][0];
									tabTempo[place][1]=voisin[k][1]; 

									int cheminP=0;
									while((listeChemin[cheminP][0]!=-1)&&(cheminP<listeChemin.length-1)) {
										cheminP++;
									}

									for(int i=0;i<listeChemin[cheminP].length;i++) {
										listeChemin[cheminP][i]=listeCheminCopy[caseBase][i];
									}
									listeChemin[cheminP]=listeCheminCopy[caseBase].clone();

									int suivant=0;
									boolean TROUVE=true;
									while ((suivant<listeChemin[cheminP].length-1) && TROUVE) {
										
										if(listeChemin[cheminP][suivant]==-1) {
											TROUVE=false;
										}
										else {
											suivant++;
										}
									}
									
									listeChemin[cheminP][suivant]=voisin[k][0];
									listeChemin[cheminP][suivant+1]=voisin[k][1];
									
									if((voisin[k][0]==posLigCaseArr) && (voisin[k][1]==posColCaseArr) ) {
										
										fin=true;
										posCheminFinal=cheminP;
									}
									
								}
											
							}
										
						}
					}
					
					ligneAncienneCase= caseDeDepart[caseBase][0];
					colonneAncienneCase = caseDeDepart[caseBase][1];
					
				}
				else if(caseDeDepart[0][0]==-1) {
					fin=true;
					
					return null;
				}
			}
			for(int i=0;i<tabTempo.length;i++) {
				for(int k=0;k<tabTempo[0].length;k++) {
					caseDeDepart[i][k]= (int) tabTempo[i][k];
				}
			}
		}
		
		int nbCasesChemin=0;
		for(int i=0;i<listeChemin[posCheminFinal].length;i=i+2) {
			if(listeChemin[posCheminFinal][i]!=-1) {
				nbCasesChemin++;
			}
			
		}
	
		int[][] resultat = new int[nbCasesChemin][2];
		
		int avancementChemin=0;
		for(int i=0;i<resultat.length;i++) {
			resultat[i][0]=listeChemin[posCheminFinal][avancementChemin];
			resultat[i][1]=listeChemin[posCheminFinal][avancementChemin+1];
			avancementChemin=avancementChemin+2;
		}	
		return resultat;
	}

	/**
	 * 
	 * Méthode permettant de calculer un chemin détaillé (chemin entre sous-cases) à partir d'un chemin entre cases.
	 *  
	 * @param chemin Un tableau représentant un chemin de cases.
	 * @param numJoueur Le numéro du joueur pour lequel nous souaitons construire un chemin détaillé.
	 * 
	 * @return Le chemin détaillé correspondant au chemin de positions de pièces données en paramètre et pour le numéro de joueur donné.
	 */
	public int[][] calculeCheminDetaille(int[][] chemin,int numJoueur){
		if (chemin.length==1)
			return new int[0][0];
		int[][] cheminDetaille=new int[chemin.length*5][4];
		int pos=0;
		int col,lig,colS,ligS;
		for (int i=0;i<chemin.length-1;i++){
			lig=chemin[i][0];
			col=chemin[i][1];
			ligS=chemin[i+1][0];
			colS=chemin[i+1][1];
			if (ligS<lig){
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=0;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=ligS;
				cheminDetaille[pos][1]=colS;
				cheminDetaille[pos][2]=2;
				cheminDetaille[pos++][3]=1;
			}
			else if (ligS>lig){
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=2;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=ligS;
				cheminDetaille[pos][1]=colS;
				cheminDetaille[pos][2]=0;
				cheminDetaille[pos++][3]=1;
			} else if (colS<col){
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=0;
				cheminDetaille[pos][0]=ligS;
				cheminDetaille[pos][1]=colS;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=2;
			} else if (colS>col){
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=1;
				cheminDetaille[pos][0]=lig;
				cheminDetaille[pos][1]=col;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=2;
				cheminDetaille[pos][0]=ligS;
				cheminDetaille[pos][1]=colS;
				cheminDetaille[pos][2]=1;
				cheminDetaille[pos++][3]=0;
			}
		}
		cheminDetaille[pos][0]=chemin[chemin.length-1][0];
		cheminDetaille[pos][1]=chemin[chemin.length-1][1];
		cheminDetaille[pos][2]=1;
		cheminDetaille[pos++][3]=1;

		int debut=0;
		if ((numJoueur==0)&&((cheminDetaille[pos-2][2]==0)||(cheminDetaille[pos-2][3]==2))) pos--;
		if ((numJoueur==1)&&((cheminDetaille[pos-2][2]==2)||(cheminDetaille[pos-2][3]==2))) pos--;
		if ((numJoueur==2)&&((cheminDetaille[pos-2][2]==2)||(cheminDetaille[pos-2][3]==0))) pos--;
		if ((numJoueur==0)&&((cheminDetaille[1][2]==0)||(cheminDetaille[0][3]==2))) debut++;
		if ((numJoueur==1)&&((cheminDetaille[1][2]==2)||(cheminDetaille[0][3]==2))) debut++;
		if ((numJoueur==2)&&((cheminDetaille[1][2]==2)||(cheminDetaille[0][3]==0))) debut++;

		int[][] resultat=new int[pos-debut][4];
		for (int i=debut;i<pos;i++)
			for (int j=0;j<4;j++)
				resultat[i-debut][j]=cheminDetaille[i][j];
		return resultat;	
	}

	/**
	 * 
	 * Méthode retournant une copie du plateau avec des copies de ses pièces.
	 * 
	 * @return Une copie du plateau avec une copie de toutes ses pièces.
	 */
	public Plateau copy(){
		Plateau plateau=new Plateau();
		for (int ligne=0;ligne<7;ligne++)
			for (int colonne=0;colonne<7;colonne++)
				plateau.positionnePiece((this.plateau[ligne][colonne]).copy(), ligne, colonne);
		return plateau;
	}	
}
