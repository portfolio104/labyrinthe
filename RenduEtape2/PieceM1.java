package composants;

/**
 * 
 * Cette classe permet de représenter les pièces du jeu de modèle 1.
 * 
 */
public class PieceM1 extends Piece {

	/**
	 * A Faire (23Avril Sylvain terminer à tester)
	 * 
	 * Constructeur permettant de construire une pièce de modèle 1 et d'orientation 0.
	 */
	public PieceM1() {
		// A Modifier !!!
		super(1,true,false,true,false); 
	}
	/**
	 * A Faire (23Avril Sylvain terminer à tester et à verifier)
	 * 
	 * Méthode permettant de créer une copie de la pièce (un nouvelle objet Java).
	 * @return Une copie de la pièce.
	 */
	public Piece copy(){
		//Piece piece=null;
		// A Compléter
		Piece piece = new PieceM1();
		piece.setOrientation(this.getOrientationPiece());
		return piece;
	}
}
