# Le fichier infos_theme.txt contiendra trois lignes :

1- La première ligne contiendra le nom de votre thème (exemple : Thème Pokemon),

2- La deuxième ligne contiendra la liste de vos prénoms et noms (exemple : JF. Condotta, F. Hemery, A. Chmeiss),

3- La troisième ligne contiendra le liste des noms des objets avec la syntaxe suivante

    {"nomObjet0","nomObjet1",...,"nomObjet17"} 


par exemple :

    {"Grappe De Raisin","Bananes","Pomme Rouge",...,"Chou Rouge","Poire"}).


Lors du paramétrage du jeu, un thème doit être sélectionné par l'utilisateur. L'activité consiste à la création de votre thème qui sera incorporé dans une future version de la librairie graphique. Pour cela vous devez créer deux fichiers :

    sprites_theme.png et
    infos_theme.txt.

Le premier fichier devra contenir les sprites utilisés par la librairie graphique pour le thème proposé. Il devra suivre le modèle 
correspondant au fichier  ```sprites_theme_modele.png.```

# Travail à rendre

Sous Moodle, dans la section Projet Tutoré S2 vous devez déposer les fichiers suivants (un seul étudiant par groupe effectuera les dépôts du début à la fin du projet) :

    Dans le dépôt Etape1 :
        Déposez les fichiers MaDemoIG.java, sprites_theme.png et infos_theme.txt.
        Avant le lundi 26 avril 2021 (23h).

