package composants;

/**
 * 
 * Cette classe permet de représenter les différentes pièces du jeu.
 * 
 */
abstract public class Piece {

	private int modelePiece; 		// Le modèle de la pièce
	private int orientationPiece; 	// L'orientation de la pièce
	private boolean[] pointsEntree; // Les points d'entrée indice 0 pour le haut, 1 pour la droite, 2 pour le bas et 3 pour la gauche.

	/**
	 * A Faire (Quand Qui Statut)
	 * 
	 * Constructeur permettant de créer une pièce d'un modèle avec l'orientation 0.
	 * @param modelePiece Le modèle de la pièce.
	 * @param pointEntreeHaut Un booléen indiquant si la pièce a un point d'entrée en haut.
	 * @param pointEntreeDroite Un booléen indiquant si la pièce a un point d'entrée à droite.
	 * @param pointEntreeBas Un booléen indiquant si la pièce a un point d'entrée en bas.
	 * @param pointEntreeGauche Un booléen indiquant si la pièce a un point d'entrée à gauche.
	 */
	public Piece(int modelePiece,boolean pointEntreeHaut,boolean pointEntreeDroite,boolean pointEntreeBas,boolean pointEntreeGauche){
		this.modelePiece=modelePiece;
		this.orientationPiece=0;
		this.pointsEntree=new boolean[4];
		this.pointsEntree[0]=pointEntreeHaut;
		this.pointsEntree[1]=pointEntreeDroite;
		this.pointsEntree[2]=pointEntreeBas;
		this.pointsEntree[3]=pointEntreeGauche;	
	}
	
	/**
	 * Méthoide retournant un String représentant la pièce.
	 */
	@Override
	public String toString() {
		return "[m:"+modelePiece+"|o:"+orientationPiece+"|pe:"+pointsEntree[0]+" "+pointsEntree[1]+" "+pointsEntree[2]+" "+pointsEntree[3]+"]";
	}
	
	/**
	 * A Faire (23Avril Sylvain terminer à tester)
	 * 
	 * Méthode permettant de rotationner une pièce dans le sens d'une horloge.
	 */
	public void rotation(){
		
		if(this.modelePiece==0) {
			if(this.orientationPiece==0) {
				this.pointsEntree[1]=false;
				this.pointsEntree[3]=true;
				
			}
			else if(this.orientationPiece==1) {
				this.pointsEntree[2]=false;
				this.pointsEntree[0]=true;
				
			}
			else if(this.orientationPiece==2){
				this.pointsEntree[3]=false;
				this.pointsEntree[1]=true;
				
			}
			else {
				this.pointsEntree[0]=false;
				this.pointsEntree[2]=true;
			}
			
			if(this.orientationPiece+1>3) {
				this.orientationPiece=0;
			}
			else {
				this.orientationPiece++;
			}
		}
		
		if(this.modelePiece==1) {
			for(int i=0;i<this.pointsEntree.length;i++) {
				if(this.pointsEntree[i]==true) {
					this.pointsEntree[i]=false;
					
				}
				else {
					this.pointsEntree[i]=true;	
				}
			}
			if(this.orientationPiece+1>1) {
				this.orientationPiece=0;
			}
			else {
				this.orientationPiece++;
			}
		}
		
		if(this.modelePiece==2) {
			int i=0;
			while(this.pointsEntree[i]!=false) {
				if(i+1<this.pointsEntree.length) {
					i++;
				}
				else {
					i=0;
				}
				
			}
			this.pointsEntree[i]=true;
			
			if(i+1<this.pointsEntree.length) {
				this.pointsEntree[i+1]=false;
					
			}
			else {
				i=0;
				this.pointsEntree[i]=false;

			}
			if(this.orientationPiece+1>3) {
				this.orientationPiece=0;
			}
			else {
				this.orientationPiece++;
			}
				
			
			
		}
		
		
	}
	
	/**
	 * A Faire (23 avril Sylvain terminer à tester)
	 * 
	 * Méthode permettant d'orienter une pièce vers une orientation spécifique.
	 * @param orientationPiece Un entier correspondant à la nouvelle orientation de la pièce.
	 */
	public void setOrientation(int orientationPiece){
		int nbRotationPossible=0;
		
		if(this.modelePiece==1){
			nbRotationPossible=1;
		}
		else {
			nbRotationPossible=3;
		}
		
		int i=this.orientationPiece;
		while(i != orientationPiece) {
			this.rotation();
			if(i+1>nbRotationPossible) {
				i=0;
			}
			else {
				i++;
			}
		}
			
	
	}

	/**
	 * A Faire (23 avril Sylvain terminer à tester)
	 * 
	 * Méthode retournant le modèle de la pièce.
	 * @return Un entier corrspondant au modèle de la pièce.
	 */
	public int getModelePiece() {
		return this.modelePiece;
	}

	/**
	 * A Faire (23 avril Sylvain terminer à tester)
	 * 
	 * Méthode retournant l'orientation de la pièce.
	 * @return un entier retournant l'orientation de la pièce.
	 */
	public int getOrientationPiece() {
		return this.orientationPiece;
	}

	/**
	 * A Faire (23 avril Sylvain terminer à tester)
	 * 
	 * Méthode indiquant si il existe un point d'entrée à une certaine position (0: en haut, 1: à droite, 2: en bas, 3: à gauche).
	 * @param pointEntree L'indice/la position du point d'entrée.
	 * @return true si il y a un point d'entrée, sinon false.
	 */
	public boolean getPointEntree(int pointEntree){
		return this.pointsEntree[pointEntree];
	}
	
	/**
	 * A Faire (23 avril Sylvain terminer à tester)
	 * 
	 * Méthode permettant de créer un tableau contenant toutes les pièces du jeu (les 50 pièces).
	 * Le tableau contiendra 20 pièces du modèle 0, 12 pièces du modèle 1 et 18 pièces du modèle 2.
	 * L'orientation de chaque pièce sera aléatoire.
	 * @return Un tableau contenant toutes les pièces du jeu.
	 */
	public static Piece[] nouvellesPieces(){
		int [] nbPieceParModele = new int[3];
		nbPieceParModele[0]=20;
		nbPieceParModele[1]=12;
		nbPieceParModele[2]=18;
		int placeTab=0;

		int[] tabPlace=Utils.genereTabIntAleatoirement(50);

		Piece pieces[]= new Piece[50];
		
		for(int i=0;i<3;i++) {
			int nbPieceCree=0;
			while(nbPieceCree != nbPieceParModele[i]) {
				if(i==0) {
					pieces[tabPlace[placeTab]]=new PieceM0();
					pieces[tabPlace[placeTab]].setOrientation(Utils.genererEntier(3));
					
				}
				else if(i==1) {
					pieces[tabPlace[placeTab]]=new PieceM1();
					pieces[tabPlace[placeTab]].setOrientation(Utils.genererEntier(1));


				}
				else {
					pieces[tabPlace[placeTab]]=new PieceM2();
					pieces[tabPlace[placeTab]].setOrientation(Utils.genererEntier(3));


				}
				
				placeTab++;
				nbPieceCree++;
				
			}
			
		}
		return pieces;
	}
	
	/**
	 * Méthode permettant de créer une copie de la pièce (un nouvelle objet Java).
	 * @return Une copie de la pièce.
	 */
	public abstract Piece copy();
}
