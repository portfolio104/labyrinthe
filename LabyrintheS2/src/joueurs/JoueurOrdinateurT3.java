package joueurs;


import java.util.Vector;


import composants.Objet;
import composants.Piece;
import composants.Utils;
import partie.ElementsPartie;


/**
 * 
 * Cette classe permet de représenter un joueur ordinateur de type T3.
 * 
 * @author Jean-François Condotta - 2021
 *
 */

public class JoueurOrdinateurT3 extends JoueurOrdinateur {

	/**
	 * Constructeur permettant de créer un joueur.
	 * 
	 * @param numJoueur Le numéro du joueur.
	 * @param nomJoueur Le nom du joueur.
	 * @param numeroImagePersonnage Le numéro de l'image représentant le joueur.
	 * @param posLignePlateau La ligne du plateau sur laquelle est positionnée le joueur.
	 * @param posColonnePlateau La colonne du plateau sur laquelle est positionnée le joueur.

	 */
	public JoueurOrdinateurT3(int numJoueur,String nomJoueur, int numeroImagePersonnage,int posLignePlateau,int posColonnePlateau) {
				super(numJoueur,nomJoueur, numeroImagePersonnage,posLignePlateau,posColonnePlateau);
	}

	@Override
	public String getCategorie() {
		return "OrdiType3";
	}
	
	@Override
	public int[] choisirOrientationEntree(ElementsPartie elementsPartie) {
		
		Joueur perso = null;
		int[] retour = new int[2];
		
		for(int joueur=0;joueur<elementsPartie.getJoueurs().length;joueur++) {
			if(elementsPartie.getJoueurs()[joueur].getNumJoueur()==this.getNumJoueur()) {
				perso = elementsPartie.getJoueurs()[joueur];
			}
		}
		
		// maintenant on regarde si on peut atteindre notre objet 
		
		Objet prochainObjet = perso.getProchainObjet();
		
		/*
		 * le cas ou il existe deja un chemin entre le joueur et le prochain Objet
		 */
		
		if(elementsPartie.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),prochainObjet.getPosLignePlateau(),prochainObjet.getPosColonnePlateau())!=null){
			// ici , notre joueur peut atteindre son objet donc, il faut éviter à tout prix un déplacement sur la ligne ou la colonne de l'objet
			// ligne objet prochainObjet.getPosLignePlateau(), colonne prochainObjet.getPosColonnePlateau()
			// pour sa colonne : il aura la fleche prochainObjet.getPosColonnePlateau() et la fleche 20 - fleche prochainObjet.getPosColonnePlateau()
			// pour sa ligne : il aura la fleche 7+prochainObjet.getPosLignePlateau() et 27 - prochainObjet.getPosLignePlateau()
			// donc il ne doit pas casser son chemin 
			// il a deja la posibilité d'acceder à son objet il va donc essayer de bloquer l'adversaire 

			int[][] chemin = elementsPartie.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),prochainObjet.getPosLignePlateau(),prochainObjet.getPosColonnePlateau());
			
			Vector<Integer> listeFlecheIndisponible = new Vector<Integer>();
			
			for(int i=0;i<chemin.length;i++) {
				listeFlecheIndisponible.add(prochainObjet.getPosColonnePlateau());
				listeFlecheIndisponible.add(20 - prochainObjet.getPosColonnePlateau());
				listeFlecheIndisponible.add(7 + prochainObjet.getPosLignePlateau()); 
				listeFlecheIndisponible.add(27 - prochainObjet.getPosLignePlateau()); 
			}
			
			// on a donc dans listeCheminIndisponible les fleches dont il ne faut pas toucher 
			
			if(elementsPartie.getNombreJoueurs()==3) {
				// on essaye de bloquer le joueur qui joue 2 tour après car ça ne sert pas de boquer le joueur suivant, car il à juste a faire l'inverse
				// du dépllacement pour avoir son objet 
				int prochainJoueur = 0 ;
				if(perso.getNumJoueur()+2<=2) {
					prochainJoueur=perso.getNumJoueur()+2;
				}
				else if(perso.getNumJoueur()==1) {
					prochainJoueur = 0;
				}
				else {
					prochainJoueur = 1;
				}
				
				Objet objetJoueur = elementsPartie.getJoueurs()[prochainJoueur].getProchainObjet();
				int[][] cheminJ = elementsPartie.getPlateau().calculeChemin(elementsPartie.getJoueurs()[prochainJoueur].getPosLigne(),elementsPartie.getJoueurs()[prochainJoueur].getPosColonne(),objetJoueur.getPosLignePlateau(),objetJoueur.getPosColonnePlateau());
				
				if(cheminJ != null) {
					Vector<Integer> listeFlecheIndisponibleVoisin = new Vector<Integer>();
					
					for(int i=0;i<chemin.length;i++) {
						listeFlecheIndisponibleVoisin.add(objetJoueur.getPosColonnePlateau());
						listeFlecheIndisponibleVoisin.add(20 - objetJoueur.getPosColonnePlateau());
						listeFlecheIndisponibleVoisin.add(7 + objetJoueur.getPosLignePlateau()); 
						listeFlecheIndisponibleVoisin.add(27 - objetJoueur.getPosLignePlateau()); 
					}
					
					for(int element=0;element<listeFlecheIndisponibleVoisin.size();element++) {
						if(listeFlecheIndisponible.contains(listeFlecheIndisponibleVoisin.get(element))==false) {
							retour[0]=elementsPartie.getPieceLibre().getOrientationPiece();
							retour[1]=listeFlecheIndisponibleVoisin.get(element);
							return retour;
						}
					}
					
					
					
				}
			}
		}
		/*
		 * ici, il n'y a pas de chemin entre l'objet et le joueur 
		 */
		else {
			for(int choixFleche=0;choixFleche<28;choixFleche++) {
				// pour chaque fleche du plateau
				// on recupere l'orientation de la piece
				
				int modelePieceLibre = elementsPartie.getPieceLibre().getModelePiece();
				int nbMaxRotation = 0;
				
				if(modelePieceLibre==1) {
					nbMaxRotation = 1;
				}
				else {
					nbMaxRotation = 3;
				}
				Piece pieceLibreCopy = elementsPartie.getPieceLibre().copy();
				
				for(int possibilite=0;possibilite<nbMaxRotation;possibilite++) {
					ElementsPartie ePCopy = elementsPartie.copy();
					
					if(possibilite!=0) {
						pieceLibreCopy.rotation();
						ePCopy.insertionPieceLibre(choixFleche);
						if(ePCopy.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),prochainObjet.getPosLignePlateau(),prochainObjet.getPosColonnePlateau()) !=null) {
							retour[0] = pieceLibreCopy.getOrientationPiece();
							retour[1] = choixFleche;
							return retour;
						}
					}
					else {
						if(ePCopy.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),prochainObjet.getPosLignePlateau(),prochainObjet.getPosColonnePlateau()) !=null) {
							retour[0] = pieceLibreCopy.getOrientationPiece();
							retour[1] = choixFleche;
							return retour;
						}
						
					}
				}
			}
			
			// si on arrive ici, on ne peut pas obtenir de chemin en deplacant une case
			// on essaye donc de bloquer l'adversaire
			
			if(elementsPartie.getNombreJoueurs()==3) {
				// on essaye de bloquer le joueur qui joue 2 tour après car ça ne sert pas de boquer le joueur suivant, car il à juste a faire l'inverse
				// du dépllacement pour avoir son objet 
				int prochainJoueur = 0 ;
				if(perso.getNumJoueur()+2<=2) {
					prochainJoueur=perso.getNumJoueur()+2;
				}
				else if(perso.getNumJoueur()==1) {
					prochainJoueur = 0;
				}
				else {
					prochainJoueur = 1;
				}
				
				Objet objetJoueur = elementsPartie.getJoueurs()[prochainJoueur].getProchainObjet();
				int[][] cheminJ = elementsPartie.getPlateau().calculeChemin(elementsPartie.getJoueurs()[prochainJoueur].getPosLigne(),elementsPartie.getJoueurs()[prochainJoueur].getPosColonne(),objetJoueur.getPosLignePlateau(),objetJoueur.getPosColonnePlateau());
				
				if(cheminJ != null) {
					retour[0] = elementsPartie.getPieceLibre().getOrientationPiece();
					if(elementsPartie.getJoueurs()[prochainJoueur].getPosLigne()==objetJoueur.getPosLignePlateau()) {
						retour[1] = objetJoueur.getPosColonnePlateau();
					}
					else {
						retour[1] = 7 + objetJoueur.getPosLignePlateau();

					}
					
					return retour;
					
					
					
					
				}
			}
			
		}
		// Travail à faire : verfier que l'empechement fonctionne 
		
		// ici, on a pas de de chemin même en essaynt d'en creer un 
		retour[0] = elementsPartie.getPieceLibre().getOrientationPiece();
		int a = Utils.genererEntier(27);
		retour[1] = a;
		
		
		return retour;
		
	}
	
	@Override
	public int[] choisirCaseArrivee(ElementsPartie elementsPartie) {
		
		Joueur perso = null;
		int[] retour = new int[2];
		
		for(int joueur=0;joueur<elementsPartie.getJoueurs().length;joueur++) {
			if(elementsPartie.getJoueurs()[joueur].getNumJoueur()==this.getNumJoueur()) {
				perso = elementsPartie.getJoueurs()[joueur];
			}
		}
		
		Objet prochainObjet = perso.getProchainObjet();
		
		if(elementsPartie.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),prochainObjet.getPosLignePlateau(),prochainObjet.getPosColonnePlateau())!=null){
			retour[0]= prochainObjet.getPosLignePlateau();
			retour[1]= prochainObjet.getPosColonnePlateau();
			return retour;
		}
		
		
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				int[][] rep = elementsPartie.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),i,j);
				if(rep !=null) {
					
					retour[0]= i ;
					retour[1]=j;
					return retour;
				}
				
				if(perso.getPosLigne()==i && perso.getPosColonne()==j) {
					retour[0]= i ;
					retour[1]=j;
					return retour;
				}
				
			}
		}
		
		return null;
		
	}

	
	@Override
	public Joueur copy(Objet objets[]){
		Joueur nouveauJoueur=new JoueurOrdinateurT3(getNumJoueur(),getNomJoueur(), getNumeroImagePersonnage(),getPosLigne(),getPosColonne());
		nouveauJoueur.setObjetsJoueur(this.getObjetsJoueurGeneral(objets));
		while (nouveauJoueur.getNombreObjetsRecuperes()!=this.getNombreObjetsRecuperes())
			nouveauJoueur.recupererObjet();
		return nouveauJoueur;
	}

}