package joueurs;

import composants.Objet;

import grafix.interfaceGraphique.IG;
import partie.ElementsPartie;
import composants.Utils;

/**
 * 
 * Cette classe permet de représenter un joueur ordinateur de type T1.
 * 
 * @author Jean-François Condotta - 2021
 *
 */

@SuppressWarnings("unused")
public class JoueurOrdinateurT1 extends JoueurOrdinateur {

	/**
	 * 
	 * Constructeur permettant de créer un joueur.
	 * 
	 * @param numJoueur Le numéro du joueur.
	 * @param nomJoueur Le nom du joueur.
	 * @param numeroImagePersonnage Le numéro de l'image représentant le joueur.
	 * @param posLignePlateau La ligne du plateau sur laquelle est positionnée le joueur.
	 * @param posColonnePlateau La colonne du plateau sur laquelle est positionnée le joueur.

	 */
	public JoueurOrdinateurT1(int numJoueur,String nomJoueur, int numeroImagePersonnage,int posLignePlateau,int posColonnePlateau) {
				super(numJoueur,nomJoueur, numeroImagePersonnage,posLignePlateau,posColonnePlateau);
	}

	@Override
	public String getCategorie() {
		return "OrdiType1";
	}
	
	@Override
	public int[] choisirOrientationEntree(ElementsPartie elementsPartie) {
		int orientation = 0;
		int a = Utils.genererEntier(27);
		int[] rep = new int[2];
		rep[0]=orientation;
		rep[1]=a;
		return rep;
		
		
	}
	
	@Override
	public int[] choisirCaseArrivee(ElementsPartie elementsPartie) {
		
		Joueur perso = null;
		int[] retour = new int[2];
		
		for(int joueur=0;joueur<elementsPartie.getJoueurs().length;joueur++) {
			if(elementsPartie.getJoueurs()[joueur].getNumJoueur()==this.getNumJoueur()) {
				perso = elementsPartie.getJoueurs()[joueur];
			}
		}
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				int[][] rep = elementsPartie.getPlateau().calculeChemin(perso.getPosLigne(),perso.getPosColonne(),i,j);
				if(rep !=null) {
					
					retour[0]= i ;
					retour[1]=j;
					return retour;
				}
				
				if(perso.getPosLigne()==i && perso.getPosColonne()==j) {
					retour[0]= i ;
					retour[1]=j;
					return retour;
				}
				
			}
		}
		
		return null;
		
	}

	
	@Override
	public Joueur copy(Objet objets[]){
		Joueur nouveauJoueur=new JoueurOrdinateurT1(getNumJoueur(),getNomJoueur(), getNumeroImagePersonnage(),getPosLigne(),getPosColonne());
		nouveauJoueur.setObjetsJoueur(this.getObjetsJoueurGeneral(objets));
		while (nouveauJoueur.getNombreObjetsRecuperes()!=this.getNombreObjetsRecuperes())
			nouveauJoueur.recupererObjet();
		return nouveauJoueur;
	}

}