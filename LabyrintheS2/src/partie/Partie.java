package partie;

import composants.Objet;



import composants.Piece;
import composants.Plateau;
import grafix.interfaceGraphique.IG;
import joueurs.Joueur;
import joueurs.JoueurOrdinateur;

@SuppressWarnings("unused")
public class Partie {
	static double version=0.0;
	private ElementsPartie elementsPartie; // Les Ã©lÃ©ments de la partie.

	/**
	 * 
	 * A Faire (2juin sylvain Statut)
	 * 
	 * Constructeur permettant de crÃ©er et d'initialiser une nouvelle partie.
	 */
	public Partie(){
		// Initialisation de la partie
		parametrerEtInitialiser();

		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,this.elementsPartie.getPlateau().getPiece(i, j).getModelePiece(),this.elementsPartie.getPlateau().getPiece(i, j).getOrientationPiece());
			}
		}
		
		
		IG.changerPieceHorsPlateau(elementsPartie.getPieceLibre().getModelePiece(), elementsPartie.getPieceLibre().getOrientationPiece());
		
		for(int i =0;i<elementsPartie.getNombreJoueurs();i++) {
			int numImageJoueur0=elementsPartie.getJoueurs()[i].getNumeroImagePersonnage(); // tout les 3 images diff
			String nomJoueur0=elementsPartie.getJoueurs()[i].getNomJoueur(); // tout les +3, on change de joueur 
			String categorieJoueur0=elementsPartie.getJoueurs()[i].getCategorie(); // tout les +3, on change de catégorie joueur suivant 
			IG.changerNomJoueur(i, nomJoueur0+" ("+categorieJoueur0+")");
			IG.changerImageJoueur(i,numImageJoueur0);
			IG.miseAJourAffichage();
		}
		for(int i=0;i<this.elementsPartie.getJoueurs().length;i++) {
			for(int objet=0;objet<this.elementsPartie.getJoueurs()[i].getObjetsJoueur().length;objet++) {
				IG.changerObjetJoueur(this.elementsPartie.getJoueurs()[i].getNumJoueur(), this.elementsPartie.getJoueurs()[i].getObjetsJoueur()[objet].getNumeroObjet(), objet);
			}
		}
		//Placer joueur
		for(int i =1;i<this.elementsPartie.getNombreJoueurs()+1;i++) {
			IG.placerJoueurPrecis(i-1, ((i*2)/6)*6, ((i)/2)*6, 2*(i/2), 2*(1-i/3));
			
		}
		Objet[] tab=elementsPartie.getObjets();
		for(int objet=0;objet<tab.length;objet++) {
			IG.placerObjetPlateau(tab[objet].getNumeroObjet(), tab[objet].getPosLignePlateau(), tab[objet].getPosColonnePlateau());
		}
		IG.miseAJourAffichage();
		
		
		// fin changement nom joueur
		IG.rendreVisibleFenetreJeu();
		
		String messageDebut[]={
				"",
				"",
				"Cliquer pour Commencer",
				""
		};
		
		IG.afficherMessage(messageDebut); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu 
		IG.attendreClic();
	}

	/**
	 * MÃ©thode permettant de paramÃ¨trer et initialiser les Ã©lÃ©ments de la partie.
	 */
	private void parametrerEtInitialiser(){
		// Saisie des diffÃ©rents paramÃ¨tres
		Object parametresJeu[];
		parametresJeu=IG.saisirParametres();
		int nombreJoueurs=((Integer)parametresJeu[0]).intValue();
		IG.creerFenetreJeu("- Version "+version, nombreJoueurs);
		// CrÃ©ation des joueurs
		Joueur joueurs[]=Joueur.nouveauxJoueurs(parametresJeu);
		// CrÃ©ation des Ã©lÃ©ments de la partie
		elementsPartie=new ElementsPartie(joueurs);
	}

	public void miseAJourPlateau() {
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,this.elementsPartie.getPlateau().getPiece(i, j).getModelePiece(),this.elementsPartie.getPlateau().getPiece(i, j).getOrientationPiece());
			}
		}
		
		IG.changerPieceHorsPlateau(this.elementsPartie.getPieceLibre().getModelePiece(), this.elementsPartie.getPieceLibre().getOrientationPiece());
		
		IG.miseAJourAffichage();
	}
	
	public void miseAJourObjet(ElementsPartie a ) {
		for(int objet=0;objet<this.elementsPartie.getObjets().length;objet++) {
			if(a.getObjets()[objet].getPosLignePlateau()!=-1) {
				IG.enleverObjetPlateau(a.getObjets()[objet].getPosLignePlateau(), a.getObjets()[objet].getPosColonnePlateau());

			}
		}
		
		for(int objet=0;objet<this.elementsPartie.getObjets().length;objet++) {
			if(this.elementsPartie.getObjets()[objet].getPosLignePlateau()!=-1) {
				IG.placerObjetPlateau(this.elementsPartie.getObjets()[objet].getNumeroObjet(), this.elementsPartie.getObjets()[objet].getPosLignePlateau(), this.elementsPartie.getObjets()[objet].getPosColonnePlateau());

			}
		}
		
		IG.miseAJourAffichage();
	}
	
	public void miseAJourJoueur() {
		for(int i=0;i<this.elementsPartie.getJoueurs().length;i++) {
			IG.placerJoueurSurPlateau(this.elementsPartie.getJoueurs()[i].getNumJoueur(), this.elementsPartie.getJoueurs()[i].getPosLigne(), this.elementsPartie.getJoueurs()[i].getPosColonne());
		}
	}
	
	/**
	 * 
	 * A Faire (2juin sylvain Statut)
	 * 
	 * MÃ©thode permettant de lancer une partie.
	 */
	public void lancer(){
		
		ElementsPartie ancien = this.elementsPartie.copy();
		boolean gagne=false;
		int tourJ;
		while(gagne == false) {
			
			for(int joueur=0; joueur<this.elementsPartie.getNombreJoueurs();joueur++) {
				
				tourJ=joueur;
				IG.changerObjetSelectionne(this.elementsPartie.getJoueurs()[joueur].getProchainObjet().getNumeroObjet());
				String messageJeu[]={
						"",
						"Au tour de " + this.elementsPartie.getJoueurs()[joueur].getNomJoueur() ,
						"Choisir une flèche ...",
						""
				};
				
				IG.afficherMessage(messageJeu); // On change de message de la fenêtre de jeu
				IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu 
				
				int[] choixFleche = this.elementsPartie.getJoueurs()[joueur].choisirOrientationEntree(this.elementsPartie);
				IG.deselectionnerFleche();
				this.elementsPartie.getPieceLibre().setOrientation(choixFleche[0]);	
				this.elementsPartie.insertionPieceLibre(choixFleche[1]);
				miseAJourPlateau();
				
				int n=0;
				int nn=0;
				int nnn=0;
				for(int i=0; i<7;i++) {
					for(int j=0;j<7;j++) {
						int a = this.elementsPartie.getPlateau().getPiece(i,j). getModelePiece();
						if(a==0) {
							n++;
						}
						else if(a==1) {
							nn++;
						}
						else {
							nnn++;
						}
						 
					}
				}
				
				System.out.println("piece 0 : " + n +"piece 1 : " + nn +"piece 2 : " + nnn );

				miseAJourJoueur();
				
				miseAJourObjet(ancien);
				ancien = this.elementsPartie.copy();
				IG.miseAJourAffichage();
				
				String messageCase[]={
						"",
						"Au tour de " + this.elementsPartie.getJoueurs()[joueur].getNomJoueur() ,
						"Choisir une case ...",
						""
				};
				
				IG.afficherMessage(messageCase); // On change de message de la fenêtre de jeu
				IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu 
				int[] choixCase = this.elementsPartie.getJoueurs()[joueur].choisirCaseArrivee(this.elementsPartie);
				
				int[][] tabCheminDispo=this.elementsPartie.getPlateau().calculeChemin(this.elementsPartie.getJoueurs()[joueur].getPosLigne(),this.elementsPartie.getJoueurs()[joueur].getPosColonne(),choixCase[0],choixCase[1]);
			
				boolean trouve = false;
				
				if(choixCase[0]==this.elementsPartie.getJoueurs()[joueur].getPosLigne() && choixCase[1]==this.elementsPartie.getJoueurs()[joueur].getPosColonne()) {
					trouve = true;
				}
				while((tabCheminDispo == null) && (trouve == false)) {
					String messageErreur[]={
							"",
							"Pas de chemin entre les cases !" ,
							"Recommnecer " + this.elementsPartie.getJoueurs()[joueur].getNomJoueur(),
							""
					};
					
					IG.afficherMessage(messageErreur); // On change de message de la fenêtre de jeu
					IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu 
					choixCase = this.elementsPartie.getJoueurs()[joueur].choisirCaseArrivee(elementsPartie);
					tabCheminDispo=this.elementsPartie.getPlateau().calculeChemin(this.elementsPartie.getJoueurs()[joueur].getPosLigne(),this.elementsPartie.getJoueurs()[joueur].getPosColonne(),choixCase[0],choixCase[1]);
					if(choixCase[0]==this.elementsPartie.getJoueurs()[joueur].getPosLigne() && choixCase[1]==this.elementsPartie.getJoueurs()[joueur].getPosColonne()) {
						trouve = true;
					}
				}
					
				if(tabCheminDispo != null) {
					this.elementsPartie.getJoueurs()[joueur].setPosition(tabCheminDispo[tabCheminDispo.length-1][0], tabCheminDispo[tabCheminDispo.length-1][1]);
					IG.placerJoueurSurPlateau(this.elementsPartie.getJoueurs()[joueur].getNumJoueur(), tabCheminDispo[tabCheminDispo.length-1][0], tabCheminDispo[tabCheminDispo.length-1][1]);
					for(int i=0;i<tabCheminDispo.length;i++) {
						IG.placerBilleSurPlateau(tabCheminDispo[i][0], tabCheminDispo[i][1], 1, 1, 0);
						IG.miseAJourAffichage();
						IG.pause(100);
						IG.supprimerBilleSurPlateau(tabCheminDispo[i][0], tabCheminDispo[i][1], 1, 1);
					}
					
				}
				else {
					IG.placerBilleSurPlateau(this.elementsPartie.getJoueurs()[joueur].getPosLigne(), this.elementsPartie.getJoueurs()[joueur].getPosColonne(), 1, 1, 0);
					IG.miseAJourAffichage();
					IG.pause(100);
					IG.supprimerBilleSurPlateau(this.elementsPartie.getJoueurs()[joueur].getPosLigne(), this.elementsPartie.getJoueurs()[joueur].getPosColonne(), 1, 1);
				}
					
				IG.miseAJourAffichage();
				IG.deselectionnerPiecePlateau();
					
				
					
					

				
				
				
				if(this.elementsPartie.getJoueurs()[joueur].getProchainObjet()!=null) {
					if(this.elementsPartie.getJoueurs()[joueur].getProchainObjet().getPosLignePlateau()==this.elementsPartie.getJoueurs()[joueur].getPosLigne() && this.elementsPartie.getJoueurs()[joueur].getProchainObjet().getPosColonnePlateau()==this.elementsPartie.getJoueurs()[joueur].getPosColonne()) {
						IG.changerObjetJoueurAvecTransparence(this.elementsPartie.getJoueurs()[joueur].getNumJoueur(), this.elementsPartie.getJoueurs()[joueur].getProchainObjet().getNumeroObjet(), this.elementsPartie.getJoueurs()[joueur].getNombreObjetsRecuperes());
						this.elementsPartie.getJoueurs()[joueur].getProchainObjet().enleveDuPlateau();
						this.elementsPartie.getJoueurs()[joueur].recupererObjet();
						IG.enleverObjetPlateau(this.elementsPartie.getJoueurs()[joueur].getPosLigne(), this.elementsPartie.getJoueurs()[joueur].getPosColonne());
						IG.miseAJourAffichage();
					}
					
				}
				
				if(this.elementsPartie.getJoueurs()[joueur].getNombreObjetsRecuperes()==6) {
					gagne=true;
				}
				
				if(gagne) {
					String messageVictoire[]={
							"",
							"Victoire de  " +this.elementsPartie.getJoueurs()[tourJ].getNomJoueur(),
							"cliquer pour quitter ...",
							""
					};
					
					IG.afficherMessage(messageVictoire); // On change de message de la fenêtre de jeu
					IG.afficherGagnant(joueur);
					IG.miseAJourAffichage();
					IG.attendreClic();
					IG.fermerFenetreJeu();
					System.exit(0);
				}
				
			}
		}
	}

	/**
	 * 
	 * Programme principal permettant de lancer le jeu.
	 * 
	 * @param args Les arguments du programmes.
	 */
	public static void main(String[] args) {
		while(true) {
			Partie partie=new Partie();
			partie.lancer();
		}
		
		
	}

}
