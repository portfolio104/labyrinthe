package partie;

import composants.Objet;

import composants.Piece;
import composants.Plateau;
import joueurs.Joueur;
import composants.Utils;

/**
 * 
 * Cette classe permet de représenter un ensemble d'élements composant une partie de jeu.
 * 
 */
public class ElementsPartie {

	private Joueur[] joueurs; 	// Les joueurs de la partie.
	private Objet[] objets; 	// Les 18 objets de la partie dans l'ordre de leurs numéros.
	private Plateau plateau; 	// Le plateau des pièces.
	private Piece pieceLibre; 	// La pièce libre.
	private int nombreJoueurs; 	// Le nombre de joueurs.

	/**
	 * 
	 * A Faire (30 mai Sylvain à tester)
	 *  
	 * Constructeur permettant de générer et d'initialiser l'ensemble des éléments d'une partie (sauf les joueurs qui sont donnés en paramètres).
	 * 
	 * Un plateau est créé en placant 49 oièces de manière aléatoire (utilisation de la méthode placerPiecesAleatoierment de la classe Plateau).
	 * La pièce restante (celle non présente sur le plateau) est affectée �  la pièce libre.
	 * Les 18 objets sont créés avec des positions aléatoires sur le plateau (utilisation de la méthode Objet.nouveauxObjets)
	 * et distribuées aux différents joueurs (utilisation de la méthode attribuerObjetsAuxJoueurs).
	 * 
	 * @param joueurs Les joueurs de la partie. Les objets des joueurs ne sont pas encore attribués (c'est au constructeur de le faire).
	 */
	public ElementsPartie(Joueur[] joueurs) {
		this.plateau=new Plateau();
		this.pieceLibre = this.plateau.placerPiecesAleatoierment();
		this.objets = Objet.nouveauxObjets();
		this.nombreJoueurs=joueurs.length;
		this.joueurs=new Joueur[nombreJoueurs];
		for(int i=0;i<nombreJoueurs;i++) {
			this.joueurs[i] = joueurs[i]; //new Joueur(joueurs[i].getNumJoueur(),joueurs[i].getNomJoueur(),joueurs[i].getNumeroImagePersonnage(),joueurs[i].getPosLigne(),joueurs[i].getPosColonne()); 
		}
		
		
		this.attribuerObjetsAuxJoueurs();
		
		
	}


	/**
	 * Un simple constructeur.
	 * 
	 * @param joueurs Les joueurs de la partie.
	 * @param objets Les 18 objets de la partie.
	 * @param plateau Le plateau de jeu.
	 * @param pieceLibre La pièce libre (la pièce hors plateau).
	 */
	public ElementsPartie(Joueur[] joueurs,Objet[] objets,Plateau plateau,Piece pieceLibre) {
		this.joueurs=joueurs;
		nombreJoueurs=joueurs.length;
		this.objets=objets;
		this.plateau=plateau;
		this.pieceLibre=pieceLibre;
	}

	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant d'attribuer les objets aux différents joueurs de manière aléatoire.
	 */
	private void attribuerObjetsAuxJoueurs(){
		int place=0;
		int[] tabPlacement = Utils.genereTabIntAleatoirement(18);
		for(int joueur=0;joueur<this.joueurs.length;joueur++) {
			Objet[] tabObjet = new Objet[6];
			tabObjet[0] = this.objets[tabPlacement[place]];
			tabObjet[1] = this.objets[tabPlacement[place+1]];
			tabObjet[2] = this.objets[tabPlacement[place+2]];
			tabObjet[3] = this.objets[tabPlacement[place+3]];
			tabObjet[4] = this.objets[tabPlacement[place+4]];
			tabObjet[5] = this.objets[tabPlacement[place+5]];
			place = place+6;
			this.joueurs[joueur].setObjetsJoueur(tabObjet);
		}
		
	}

	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant de récupérer les joueurs de la partie.
	 * @return Les joueurs de la partie.
	 */
	public Joueur[] getJoueurs() {
		return this.joueurs;
	}


	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant de récupérer les pièces de la partie.
	 * @return Les objets de la partie.
	 */
	public Objet[] getObjets() {
		return this.objets;
	}


	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant de récupérer le plateau de pièces de la partie.
	 * @return Le plateau de pièces.
	 */
	public Plateau getPlateau() {
		return this.plateau;
	}


	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant de récupérer la pièce libre de la partie.
	 * @return La pièce libre.
	 */
	public Piece getPieceLibre() {
		return this.pieceLibre;
	}


	/**
	 * A Faire (30 mai Sylvain à tester)
	 * 
	 * Méthode permettant de récupérer le nombre de joueurs de la partie.
	 * @return Le nombre de joueurs.
	 */
	public int getNombreJoueurs() {
		return this.nombreJoueurs;
	}


	/**
	 * A Faire (31 mai Sylvain à tester)
	 * 
	 * Méthode modifiant les différents éléments de la partie suite �  l'insertion de la pièce libre dans le plateau.
	 * 
	 * @param choixEntree L'entrée choisie pour réaliser l'insertion (un nombre entre 0 et 27).
	 */
	public void insertionPieceLibre(int choixEntree){
		/*
		 * de 0 à 6 les fléches du haut : on recupere la  pièce [6,numFleche] et la fleche en dehors prend la place [0,numFleche]
		 * de 7 à 13 les fléches de droite : on recupere la  pièce [numFleche,0] et la fleche en dehors prend la place [numFleche,6]
		 * de 14 à 20 les flèches du bas : on recupere la  pièce [0,numFleche] et la fleche en dehors prend la place [6,numFleche]
		 * de 21 à 27 les flèches de gauche : on recupere la  pièce [numFleche,6] et la fleche en dehors prend la place [numFleche,0]
		 */
		int[][] valeur = {{0,1,2,3,4,5,6},{7,8,9,10,11,12,13},{14,15,16,17,18,19,20},{21,22,23,24,25,26,27}};
		int[][] valeurC = {{0,1,2,3,4,5,6},{0,1,2,3,4,5,6},{6,5,4,3,2,1,0},{6,5,4,3,2,1,0}};
		
		for(int i=0;i<valeur.length;i++) {
			for(int j=0;j<valeur[i].length;j++) {
				
				
				
				if(valeur[i][j]==choixEntree) {
					if(i==0) {
						// flèches du haut 
						choixEntree=valeurC[i][j];
						Piece recup = this.plateau.getPiece(6,choixEntree);
						this.plateau.positionnePiece(this.pieceLibre, 6, choixEntree);
						this.pieceLibre = recup;
						for(int deplacement = 6; deplacement>0;deplacement--) {
							Piece temp = this.plateau.getPiece(deplacement-1,choixEntree);
							this.plateau.positionnePiece(this.plateau.getPiece(deplacement,choixEntree), deplacement-1, choixEntree);
							this.plateau.positionnePiece(temp, deplacement, choixEntree);
						}
						
						for (int objet=0;objet<this.getObjets().length;objet++) {
							if(this.getObjets()[objet].getPosColonnePlateau()==choixEntree) {
								if(this.getObjets()[objet].getPosLignePlateau()+1>6) {
									this.getObjets()[objet].positionneObjet(0,this.getObjets()[objet].getPosColonnePlateau());

								}
								else {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau()+1,this.getObjets()[objet].getPosColonnePlateau());
								}
							}
						}
						
						for(int joueur=0;joueur<this.getNombreJoueurs();joueur++) {
							if(this.joueurs[joueur].getPosColonne()==choixEntree) {
								if(this.joueurs[joueur].getPosLigne()+1>6) {
									this.joueurs[joueur].setPosition(0, this.joueurs[joueur].getPosColonne());
								}
								else {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne()+1, this.joueurs[joueur].getPosColonne());

								}
							}
						}
					}
					
					if(i==1) {
						// flèches de droite
						choixEntree=valeurC[i][j];
						Piece recup = this.plateau.getPiece(choixEntree,0);
						this.plateau.positionnePiece(this.pieceLibre, choixEntree, 0);
						this.pieceLibre = recup;
						for(int deplacement = 0; deplacement<6;deplacement++){
							Piece temp = this.plateau.getPiece(choixEntree,deplacement+1);
							this.plateau.positionnePiece(this.plateau.getPiece(choixEntree,deplacement), choixEntree,deplacement+1);
							this.plateau.positionnePiece(temp, choixEntree,deplacement);
						}
						for (int objet=0;objet<this.getObjets().length;objet++) {
							
							if(this.getObjets()[objet].getPosLignePlateau()==choixEntree) {
								if((this.getObjets()[objet].getPosColonnePlateau()-1)<0) {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau(),6);

								}
								else {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau(),this.getObjets()[objet].getPosColonnePlateau()-1);
								}
							}
						}
						
						for(int joueur=0;joueur<this.getNombreJoueurs();joueur++) {
							if(this.joueurs[joueur].getPosLigne()==choixEntree) {
								if(this.joueurs[joueur].getPosColonne()-1<0) {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne(), 6);
								}
								else {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne(), this.joueurs[joueur].getPosColonne()-1);

								}
							}
						}
					}
					
					if(i==2) {
						// flèches du bas 
						choixEntree=valeurC[i][j];
						Piece recup = this.plateau.getPiece(0,choixEntree);
						this.plateau.positionnePiece(this.pieceLibre, 0, choixEntree);
						this.pieceLibre = recup;
						for(int deplacement = 0; deplacement<6;deplacement++){
							Piece temp = this.plateau.getPiece(deplacement+1,choixEntree);
							this.plateau.positionnePiece(this.plateau.getPiece(deplacement,choixEntree), deplacement+1, choixEntree);
							this.plateau.positionnePiece(temp, deplacement, choixEntree);
						}
						
						for (int objet=0;objet<this.getObjets().length;objet++) {
							if(this.getObjets()[objet].getPosColonnePlateau()==choixEntree) {
								if(this.getObjets()[objet].getPosLignePlateau()-1<0) {
									this.getObjets()[objet].positionneObjet(6,this.getObjets()[objet].getPosColonnePlateau());

								}
								else {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau()-1,this.getObjets()[objet].getPosColonnePlateau());
								}
							}
						}
						
						for(int joueur=0;joueur<this.getNombreJoueurs();joueur++) {
							if(this.joueurs[joueur].getPosColonne()==choixEntree) {
								if(this.joueurs[joueur].getPosLigne()-1<0) {
									this.joueurs[joueur].setPosition(6, this.joueurs[joueur].getPosColonne());
								}
								else {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne()-1, this.joueurs[joueur].getPosColonne());

								}
							}
						}
					}
					
					if(i==3) {
						// flèches du haut 
						choixEntree=valeurC[i][j];
						Piece recup = this.plateau.getPiece(choixEntree,6);
						this.plateau.positionnePiece(this.pieceLibre, choixEntree,6);
						this.pieceLibre = recup;
						for(int deplacement = 6; deplacement>0;deplacement--) {
							Piece temp = this.plateau.getPiece(choixEntree,deplacement-1);
							this.plateau.positionnePiece(this.plateau.getPiece(choixEntree,deplacement), choixEntree,deplacement-1);
							this.plateau.positionnePiece(temp,choixEntree,deplacement);
						}
						
						for (int objet=0;objet<this.getObjets().length;objet++) {
							if(this.getObjets()[objet].getPosLignePlateau()==choixEntree) {
								if(this.getObjets()[objet].getPosColonnePlateau()+1>6) {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau(),0);

								}
								else {
									this.getObjets()[objet].positionneObjet(this.getObjets()[objet].getPosLignePlateau(),this.getObjets()[objet].getPosColonnePlateau()+1);
								}
							}
						}
						
						for(int joueur=0;joueur<this.getNombreJoueurs();joueur++) {
							if(this.joueurs[joueur].getPosLigne()==choixEntree) {
								if(this.joueurs[joueur].getPosColonne()+1>6) {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne(), 0);
								}
								else {
									this.joueurs[joueur].setPosition(this.joueurs[joueur].getPosLigne(), this.joueurs[joueur].getPosColonne()+1);

								}
							}
						}
					}
				}
			}
		}
		
	}


	/**
	 * Méthode retournant une copie.
	 * 
	 * @return Une copie des éléments.
	 */
	public ElementsPartie copy(){
		Objet[] nouveauxObjets=new Objet[(this.objets).length];
		for (int i=0;i<objets.length;i++)
			nouveauxObjets[i]=(this.objets[i]).copy();
		Joueur[] nouveauxJoueurs=new Joueur[nombreJoueurs];
		for (int i=0;i<nombreJoueurs;i++)
			nouveauxJoueurs[i]=(this.joueurs[i]).copy(objets);
		Plateau nouveauPlateau=(this.plateau).copy();
		Piece nouvellePieceLibre=(this.pieceLibre).copy();
		ElementsPartie nouveauxElements=new  ElementsPartie(nouveauxJoueurs,nouveauxObjets,nouveauPlateau,nouvellePieceLibre); 
		return nouveauxElements;
	}


}
