package tests;
import composants.Objet;

import composants.Plateau;
import grafix.interfaceGraphique.IG;
import joueurs.Joueur;
import partie.ElementsPartie;

public class TestElementsPartie {
	
	public static void main(String[] args) {
		Object parametresJeu[];
		parametresJeu=IG.saisirParametres();
		int  nbJoueurs=((Integer)parametresJeu[0]).intValue();
		IG.creerFenetreJeu("- TestElementsPartie",nbJoueurs);
		Joueur joueurs[]=Joueur.nouveauxJoueurs(parametresJeu);
		ElementsPartie elementsPartie=new ElementsPartie(joueurs);
		Plateau plateau=elementsPartie.getPlateau();
		
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,plateau.getPiece(i, j).getModelePiece(),plateau.getPiece(i, j).getOrientationPiece());
			}
		}
		
		
		IG.changerPieceHorsPlateau(elementsPartie.getPieceLibre().getModelePiece(), elementsPartie.getPieceLibre().getOrientationPiece());
		
		for(int i =0;i<elementsPartie.getNombreJoueurs();i++) {
			int numImageJoueur0=elementsPartie.getJoueurs()[i].getNumeroImagePersonnage(); // tout les 3 images diff
			String nomJoueur0=elementsPartie.getJoueurs()[i].getNomJoueur(); // tout les +3, on change de joueur 
			String categorieJoueur0=elementsPartie.getJoueurs()[i].getCategorie(); // tout les +3, on change de catégorie joueur suivant 
			IG.changerNomJoueur(i, nomJoueur0+" ("+categorieJoueur0+")");
			IG.changerImageJoueur(i,numImageJoueur0);
			IG.miseAJourAffichage();
		}
		for(int i=0;i<elementsPartie.getJoueurs().length;i++) {
			for(int objet=0;objet<elementsPartie.getJoueurs()[i].getObjetsJoueur().length;objet++) {
				IG.changerObjetJoueur(elementsPartie.getJoueurs()[i].getNumJoueur(),elementsPartie.getJoueurs()[i].getObjetsJoueur()[objet].getNumeroObjet(), objet);
			}
		}	
		//Placer joueur
		for(int i =1;i<nbJoueurs+1;i++) {
			IG.placerJoueurPrecis(i-1, ((i*2)/6)*6, ((i)/2)*6, 2*(i/2), 2*(1-i/3));
			
		}
		Objet[] tab=elementsPartie.getObjets();
		
		
		for(int objet=0;objet<tab.length;objet++) {
			IG.placerObjetPlateau(tab[objet].getNumeroObjet(), tab[objet].getPosLignePlateau(), tab[objet].getPosColonnePlateau());
		}
		
		
		IG.miseAJourAffichage();
		
		
		// fin changement nom joueur
		
		
		// Affichage d'un message
		String message[]={
							"",
							"",
							"Cliquez pour Commencer ...",
							""
		};
		
		IG.afficherMessage(message); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.rendreVisibleFenetreJeu();  // On rend visible la fenêtre de jeu
		IG.attendreClic();
		ElementsPartie ancien = elementsPartie.copy();
		for(int tour=0;tour<4;tour++) {
			// Affichage d'un message
			String messageFleche[]={
					"",
					"Choissisez une entrée ...",
					"(une des flèches)",
					""
			};
			
			IG.afficherMessage(messageFleche); // On change de message de la fenêtre de jeu
			IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu

			int a = IG.attendreChoixEntree();
			elementsPartie.insertionPieceLibre(a);
			for(int i=0;i<7;i++) {
				for(int j=0;j<7;j++) {
					IG.changerPiecePlateau(i,j,elementsPartie.getPlateau().getPiece(i, j).getModelePiece(),elementsPartie.getPlateau().getPiece(i, j).getOrientationPiece());
				}
			}
			IG.changerPieceHorsPlateau(elementsPartie.getPieceLibre().getModelePiece(), elementsPartie.getPieceLibre().getOrientationPiece());
			for(int objet=0;objet<elementsPartie.getObjets().length;objet++) {
				if(ancien.getObjets()[objet].getPosLignePlateau()!=-1) {
					IG.enleverObjetPlateau(ancien.getObjets()[objet].getPosLignePlateau(), ancien.getObjets()[objet].getPosColonnePlateau());

				}
			}
			for(int objet=0;objet<elementsPartie.getObjets().length;objet++) {
				if(elementsPartie.getObjets()[objet].getPosLignePlateau()!=-1) {
					IG.placerObjetPlateau(elementsPartie.getObjets()[objet].getNumeroObjet(),elementsPartie.getObjets()[objet].getPosLignePlateau(),elementsPartie.getObjets()[objet].getPosColonnePlateau());

				}
			}
			
			for(int i=0;i<elementsPartie.getJoueurs().length;i++) {
				IG.placerJoueurSurPlateau(elementsPartie.getJoueurs()[i].getNumJoueur(), elementsPartie.getJoueurs()[i].getPosLigne(),elementsPartie.getJoueurs()[i].getPosColonne());
			}
			
			ancien=elementsPartie.copy();
			
			IG.miseAJourAffichage();
			
			
		}
		
		String messageFin[]={
				"",
				"C'est terminé !",
				"Cliquer pour quitter ...",
				""
		};
		
		IG.afficherMessage(messageFin); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu 
		IG.attendreClic();
		IG.fermerFenetreJeu();
		System.exit(0);

		
	}
}
