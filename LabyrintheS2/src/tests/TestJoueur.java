package tests;
import composants.Piece;
import composants.Plateau;
import grafix.interfaceGraphique.IG;
import joueurs.Joueur;

public class TestJoueur {
	
	public static void main(String[] args) {
		// Création de la 1ere interface dés le démarrage
		
		// Saisie des différents paramètres
		Object parametres[];
		parametres=IG.saisirParametres(); // On ouvre la fenêtre de paramétrage pour la saisie
		
		Plateau plateau=new Plateau();
		Piece pieceHorsPlateau=plateau.placerPiecesAleatoierment();
		
		Joueur joueurs[]=Joueur.nouveauxJoueurs(parametres);
		
		// Création de la fenêtre de jeu et affichage de la fenêtre 
		int nbJoueurs=((Integer)parametres[0]).intValue(); // Récupération du nombre de joueurs
		IG.creerFenetreJeu("Pouette",nbJoueurs); // On crée la fenêtre
		

		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,plateau.getPiece(i, j).getModelePiece(),plateau.getPiece(i, j).getOrientationPiece());
			}
		}
		
		IG.changerPieceHorsPlateau(pieceHorsPlateau.getModelePiece(),pieceHorsPlateau.getOrientationPiece());
		IG.miseAJourAffichage();
		
		
		// Création joueur
		for(int i =0;i<nbJoueurs;i++) {
			int numImageJoueur0=((Integer)parametres[3+3*i]).intValue(); // tout les 3 images diff
			String nomJoueur0=(String)parametres[1+3*i]; // tout les +3, on change de joueur 
			String categorieJoueur0=(String)parametres[2+3*i]; // tout les +3, on change de catégorie joueur suivant 
			IG.changerNomJoueur(i, nomJoueur0+" ("+categorieJoueur0+")");
			IG.changerImageJoueur(i,numImageJoueur0);
			IG.miseAJourAffichage();
		}
		
		//Placer joueur
		for(int i =1;i<nbJoueurs+1;i++) {
			IG.placerJoueurPrecis(i-1, ((i*2)/6)*6, ((i)/2)*6, 2*(i/2), 2*(1-i/3));
			
		}
		// fin changement nom joueur
		
		IG.jouerUnSon(2); // On joue le son numéro 2
		IG.pause(300); // On attend 300 ms
		IG.jouerUnSon(2); // On joue de nouveau le son numéro 2
		
		// Affichage d'un message
		String message[]={
							"",
							"",
							"Cliquez pour Commencer ...",
							""
		};
		
		IG.afficherMessage(message); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.rendreVisibleFenetreJeu();  // On rend visible la fenêtre de jeu
		IG.attendreClic();
		
		for (int i=0;i<nbJoueurs;i++) {
			
			String message1[]={
					"",
					"C'est au tour de "+joueurs[i].getNomJoueur(),
					"Sélectionner une case ...",
					""
			};
		


			IG.afficherMessage(message1); // On change de message de la fenêtre de jeu
			IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
			
			int[] tabCA=joueurs[i].choisirCaseArrivee(null);
		
			int[][] tabCheminDispo=plateau.calculeChemin(joueurs[i].getPosLigne(),joueurs[i].getPosColonne(),tabCA[0],tabCA[1]);
			
			boolean trouve=false;
			
			if (tabCA[0]==joueurs[i].getPosLigne() && tabCA[1]==joueurs[i].getPosColonne()) {
				trouve=true;
			}
			
			if(tabCheminDispo!=null ) {
				trouve=true;
				IG.placerJoueurPrecis(i, tabCA[0], tabCA[1],2*((i+1)/2), 2*(1-(i+1)/3));
				for(int v=0;v<tabCheminDispo.length;v++) {
					if(tabCheminDispo[v]!=null) {
						IG.placerBilleSurPlateau(tabCheminDispo[v][0],tabCheminDispo[v][1], 1, 1, 1);

					}
						
				}
			}
			
			while (!trouve){
				tabCA=joueurs[i].choisirCaseArrivee(null);
				tabCheminDispo=plateau.calculeChemin(joueurs[i].getPosLigne(),joueurs[i].getPosColonne(),tabCA[0], tabCA[1] );
				
				if (tabCA[0]==joueurs[i].getPosLigne() && tabCA[1]==joueurs[i].getPosColonne()) {
					trouve=true;
				}
				
				if (tabCheminDispo!=null) {
					trouve=true;
					IG.placerJoueurPrecis(i, tabCA[0], tabCA[1],2*((i+1)/2), 2*(1-(i+1)/3));
					
					for(int v=0;v<tabCheminDispo.length;v++) {
						if(tabCheminDispo[v]!=null) {
							IG.placerBilleSurPlateau(tabCheminDispo[v][0],tabCheminDispo[v][1], 1, 1, 1);

						}
							
					}
					
					
				}
				String message2[]={
						"",
						"C'est au tour de "+joueurs[i].getNomJoueur(),
						"Recommencez",
						""
				};
			


				IG.afficherMessage(message2); // On change de message de la fenêtre de jeu
				IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
			}		
			IG.miseAJourAffichage();
		}
		String message3[]={
				"",
				"C'est terminé !",
				"Cliquer pour terminer ... ",
				""
		};
		
		IG.afficherMessage(message3); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.attendreClic();
		IG.fermerFenetreJeu();
		System.exit(0);
	}
}