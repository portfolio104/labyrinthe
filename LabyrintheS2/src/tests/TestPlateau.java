package tests;

import composants.Piece;

import composants.Plateau;
import grafix.interfaceGraphique.IG;

public class TestPlateau {
	
	public static void main(String[] args) {
		
		// Création de la 1ere interface dés le démarrage
		
		// Saisie des différents paramètres
		Object parametres[];
		parametres=IG.saisirParametres(); // On ouvre la fenêtre de paramétrage pour la saisie
					
					
		// Création de la fenêtre de jeu et affichage de la fenêtre 
		int nbJoueurs=((Integer)parametres[0]).intValue(); // Récupération du nombre de joueurs
		IG.creerFenetreJeu("Démo Librairie IG version 1.9",nbJoueurs); // On crée la fenêtre
				
		Plateau plateau=new Plateau();
		Piece pieceHorsPlateau=plateau.placerPiecesAleatoierment();
				
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.rendreVisibleFenetreJeu();  // On rend visible la fenêtre de jeu
		int n=0;
		int nn=0;
		int nnn=0;
		for(int i=0; i<7;i++) {
			for(int j=0;j<7;j++) {
				int a = plateau.getPiece(i,j). getModelePiece();
				if(a==0) {
					n++;
				}
				else if(a==1) {
					nn++;
				}
				else {
					nnn++;
				}
				 
			}
		}
		
		System.out.println("piece 0 : " + n +"piece 1 : " + nn +"piece 2 : " + nnn );
		IG.attendreClic();  // On attend un clic de souris

		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,plateau.getPiece(i, j).getModelePiece(),plateau.getPiece(i, j).getOrientationPiece());
			}
		}
		
		IG.changerPieceHorsPlateau(pieceHorsPlateau.getModelePiece(),pieceHorsPlateau.getOrientationPiece());
		
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		
		IG.attendreClic();  // On attend un clic de souris
		
		int[][] v=null;
		int[][] tabChemin=null;
		
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				v = plateau.calculeChemin(3, 3, i, j);
				if(v != null) {
					if(tabChemin == null) {
						System.out.println("La liste des chemins trouvés à partir de la case (3,3) :");

						tabChemin=v;
					}
					else if(v.length > tabChemin.length) {
						tabChemin=v;
					}
					String chemin= "" ;
					int g=0;
					while((g<v.length) && (v[g] !=null)) {
						chemin += "(" + v[g][0] + "," + v[g][1] + ")";
						g++;
					}
					
					System.out.println("Chemin entre les cases (3,3) et (" + i + "," + j + ") : " + chemin);
					
				}
			}
			
		}
		
		if(tabChemin != null) {
			for(int i=0;i<tabChemin.length;i++) {
				if(tabChemin[i]!=null) {
					IG.placerBilleSurPlateau(tabChemin[i][0],tabChemin[i][1], 1, 1, 1);

				}
				
			}
		}
		
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.attendreClic();  // On attend un clic de souris
		IG.fermerFenetreJeu();
		System.exit(0);
	}
}
