package tests;

import composants.Objet;
import grafix.interfaceGraphique.IG;

public class TestObjet {
	
	public static void main(String[] args) {
		// Création de la 1ere interface dés le démarrage
		
		// Saisie des différents paramètres
		Object parametres[];
		parametres=IG.saisirParametres(); // On ouvre la fenêtre de paramétrage pour la saisie
			
			
		// Création de la fenêtre de jeu et affichage de la fenêtre 
		int nbJoueurs=((Integer)parametres[0]).intValue(); // Récupération du nombre de joueurs
		IG.creerFenetreJeu("Démo Librairie IG version 1.9",nbJoueurs); // On crée la fenêtre
		
		// change le sens des pièces du plateau
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,0,0);
			}
		}
		// Change la pièce hors du plateau
		IG.changerPieceHorsPlateau(0,0);
		// fin changement des sens de départ
			
		// Affichage d'un message
				String message[]={
									"",
									"Cliquer quitter ...",
									""
				};
				
		Objet[] tab = Objet.nouveauxObjets();
				
		for(int objet=0;objet<tab.length;objet++) {
			IG.placerObjetPlateau(tab[objet].getNumeroObjet(), tab[objet].getPosLignePlateau(), tab[objet].getPosColonnePlateau());
		}
				
			
		IG.afficherMessage(message); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.rendreVisibleFenetreJeu();  // On rend visible la fenêtre de jeu
		IG.attendreClic();  // On attend un clic de souris
		IG.fermerFenetreJeu();
		System.exit(0);
			
		//fin affichage de base
		
		
	}

}
