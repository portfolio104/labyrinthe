package tests;

import composants.Piece;
import composants.Plateau;

public class test {
	
	public static void main(String[] args) {
		Piece[] a = Piece.nouvellesPieces();
		
		int n=0;
		int nn=0;
		int nnn=0;
		
		for(int i=0;i<a.length;i++) {
			if(a[i].getModelePiece()==0) {
				n++;
			}
			else if(a[i].getModelePiece()==1) {
				nn++;
			}
			else {
				nnn++;
			}
		}
		
		System.out.println("piece 0 : " + n +"piece 1 : " + nn +"piece 2 : " + nnn );
		
		Plateau b = new Plateau();
		
		b.placerPiecesAleatoierment();
		
		int n1=0;
		int n2=0;
		int n3=0;
		for(int i=0; i<7;i++) {
			for(int j=0;j<7;j++) {
				int av = b.getPiece(i,j). getModelePiece();
				if(av==0) {
					n1++;
				}
				else if(av==1) {
					n2++;
				}
				else {
					n3++;
				}
				 
			}
		}
		
		System.out.println("piece 0 : " + n1 +"piece 1 : " + n2 +"piece 2 : " + n3 );
	}

}
