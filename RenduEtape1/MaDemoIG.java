package tests;
import grafix.interfaceGraphique.IG;

public class MaDemoIG {
	
	public static void main(String[] args) {
		// Création de la 1ere interface dés le démarrage
	
		// Saisie des différents paramètres
		Object parametres[];
		parametres=IG.saisirParametres(); // On ouvre la fenêtre de paramétrage pour la saisie
		
		
		// Création de la fenêtre de jeu et affichage de la fenêtre 
		int nbJoueurs=((Integer)parametres[0]).intValue(); // Récupération du nombre de joueurs
		IG.creerFenetreJeu("Démo Librairie IG version 1.9",nbJoueurs); // On crée la fenêtre
		
				
		// change le sens des pièces du plateau
		for(int i=0;i<7;i++) {
			for(int j=0;j<7;j++) {
				IG.changerPiecePlateau(i,j,2,0);
			}
		}
		// Change la pièce hors du plateau
		IG.changerPieceHorsPlateau(1,0);
		// fin changement des sens de départ
		
		// Changement des caractéristiques du premier joueur avec les paramètres saisis
		//j1
		int numImageJoueur0=((Integer)parametres[3]).intValue(); // tout les 3 images diff
		String nomJoueur0=(String)parametres[1]; // tout les +3, on change de joueur 
		String categorieJoueur0=(String)parametres[2]; // tout les +3, on change de catégorie joueur suivant 
		IG.changerNomJoueur(0, nomJoueur0+" ("+categorieJoueur0+")");
		IG.changerImageJoueur(0,numImageJoueur0);
			
		//j2
			
		int numImageJoueur1=((Integer)parametres[6]).intValue(); // tout les 3 images diff
		String nomJoueur1=(String)parametres[4]; // tout les +3, on change de joueur 
		String categorieJoueur1=(String)parametres[5]; // tout les +3, on change de catégorie joueur suivant 
		IG.changerNomJoueur(1, nomJoueur1+" ("+categorieJoueur1+")");
		IG.changerImageJoueur(1,numImageJoueur1);
			
		//j3
			
		int numImageJoueur2=((Integer)parametres[9]).intValue(); // tout les 3 images diff
		String nomJoueur2=(String)parametres[7]; // tout les +3, on change de joueur 
		String categorieJoueur2=(String)parametres[8]; // tout les +3, on change de catégorie joueur suivant 
		IG.changerNomJoueur(2, nomJoueur2+" ("+categorieJoueur2+")");
		IG.changerImageJoueur(2,numImageJoueur2);
		
		// fin changement nom joueur

		// Place des objets sur le plateau
			int numObjet=0;
			int compteur=0;
			for (int i=0;i<3;i++) {
				for (int j=0;j<7;j++) {
					if(compteur++ <=18) {
						IG.placerObjetPlateau((numObjet++)%18,i,j);
					}
				}
			}
			
		// fin placement objet
			
		// Place les joueurs sur le plateau
		
		//j1
			
	
		IG.placerJoueurPrecis(0, 3, 0, 1, 0); // param1=numJoueur, param2=ligne, param3=colonne, param4=ligne param5=colonne
		
		
		//j2
		IG.placerJoueurPrecis(1, 3, 6, 1, 2); // param1=numJoueur, param2=ligne, param3=colonne, param4=ligne param5=colonne
		
		//fin placement
		
		IG.jouerUnSon(2); // On joue le son numéro 2
		IG.pause(300); // On attend 300 ms
		IG.jouerUnSon(2); // On joue de nouveau le son numéro 2
		
		
		
		// Affichage d'un message
		String message[]={
							"",
							"Bonjour !	",
							"Cliquer pour continuer ...",
							""
		};
		
		// changement objet perso
		int numJoueur=0;
		int position=0;
		
		for(int objet=0;objet<18;objet++) {
			if((position % 5)==0 && position !=0) {
				
				IG.changerObjetJoueur(numJoueur, objet, position);
				position=0;
				numJoueur+=1;
				
			}
			else {
				IG.changerObjetJoueur(numJoueur, objet, position);
				position+=1;
			}
			
		}
		
		IG.afficherMessage(message); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.rendreVisibleFenetreJeu();  // On rend visible la fenêtre de jeu
		IG.attendreClic();  // On attend un clic de souris
		
		// fin création interface de démarrage
		
		// partie après démarrage
		
		// rotation des pièces après clic
		
		// change le sens des pièces du plateau
		int tour=1;
		
		// def place actuel perso1
		
		int j1CaseColonne=0;
		int j1CaseLigne=1;
		int j1Ligne=0;
		int j1Colonne=3;
		
		// def place actuel perso2
		
		int j2CaseColonne=2;
		int j2CaseLigne=1;
		int j2Ligne=6;
		int j2Colonne=3;
		
		//initialisation bille j1
		int j1BilleCaseColonne=0;
		int j1BilleCaseLigne=1;
		int j1BilleLigne=0;
		int j1BilleColonne=3;
		
		//initialisation bille j2
		int j2BilleCaseColonne=2;
		int j2BilleCaseLigne=1;
		int j2BilleLigne=6;
		int j2BilleColonne=3;
		
		int numBille=0;
		int numPieceHors=1;
		

		while(tour<=4) {
			IG.attendreClic();  // On attend un clic de souris
			String message1[]={
					"",
					"Après clic ",Integer. toString(tour),
					"Cliquer pour continuer ...",
					""
			};
			j1CaseColonne += 1;
			j2CaseColonne -= 1;


			IG.placerJoueurPrecis(0, j1Colonne, j1Ligne, j1CaseLigne, j1CaseColonne); // param1=numJoueur, param2=ligne, param3=colonne, param4=ligne param5=colonne
			IG.placerJoueurPrecis(1, j2Colonne, j2Ligne, j2CaseLigne, j2CaseColonne); // param1=numJoueur, param2=ligne, param3=colonne, param4=ligne param5=colonne

			IG.placerBilleSurPlateau(j1BilleColonne, j1BilleLigne, j1BilleCaseLigne, j1BilleCaseColonne,numBille);
			IG.placerBilleSurPlateau(j2BilleColonne, j2BilleLigne, j2BilleCaseLigne, j2BilleCaseColonne,numBille);

			
			if(j1BilleCaseColonne+1>2) {
				j1BilleLigne+=1;
				j1BilleCaseColonne=0;
			}
			else {
				j1BilleCaseColonne+=1;
			}
			
			if(j2BilleCaseColonne-1<0) {
				j2BilleLigne-=1;
				j2BilleCaseColonne=2;
			}
			else {
				j2BilleCaseColonne-=1;
			}
			
			if(numBille+1<4) {
				numBille+=1;
			}
			else {
				numBille=0;
			}

					
			
			IG.afficherMessage(message1); // On change de message de la fenêtre de jeu
			IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
			
			for(int i=0;i<7;i++) {
				for(int j=0;j<7;j++) {
					if(tour==4) {
						IG.changerPiecePlateau(i,j,2,0);
						
					}
					else {
						IG.changerPiecePlateau(i,j,2,tour);
					}
					
				}
			}
			
			
			// Change la pièce hors du plateau
			IG.changerPieceHorsPlateau(1,numPieceHors);
			if(numPieceHors+1>1) {
				numPieceHors=0;
			}
			else {
				numPieceHors++;
			}
			
			// fin changement des sens de départ
			IG.changerObjetJoueurAvecTransparence(0,tour-1,tour-1); // à modifier
			IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
			tour++;

			
		}
		IG.attendreClic();  // On attend un clic de souris
		IG.afficherGagnant(0);
		
		// fin des rotation avec clics
		
		String message1[]={
				"",
				"Cliquez sur une fléche ",
				"Pour quitter !!",
				""
		};
		
	
		IG.afficherMessage(message1); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		
		IG.attendreChoixEntree();
		
		String message2[]={
				"",
				"Arret du programme",
				"Dans 2 secondes !",
				""
		};
		
		IG.afficherMessage(message2); // On change de message de la fenêtre de jeu
		IG.miseAJourAffichage(); // On effectue le rafraichissement de la fenêtre de jeu
		IG.pause(2000);
		IG.fermerFenetreJeu();
		System.exit(0);
		
		
	
		
		
	}
}
