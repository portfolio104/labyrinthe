# lien http://www.cril.univ-artois.fr/~hemery/enseignement/An20-21/projetTutS2/

La classe Utils

Dans votre projet Java Labyrinthe, créez un paquetage appelé composants et placez-y le fichier Utils.java. La classe Utils contient deux méthodes statiques que vous devez compléter.

En première ligne de commentaire de chaque méthode à compléter ou à modifier se trouve une ligne de la forme :

    À Faire (Quand Qui Statut).

    Cette ligne de commentaire devra être modifiée au cours du développement pour indiquer
        la date de la dernière mise à jour de son code (Quand),
        les initiales de la personne ou des personnes qui ont effectué cette dernière mise à jour (Qui) et
        le statut du code de la méthode (Statut) qui pourra être le mot EnCours ou le mot Finalisée.

    À titre d'exemple, à un moment donné une telle ligne pourra être :

    À Faire (11/04/21 JFC EnCours)

    puis à un autre moment :

    À Faire (11/04/21 JFC Finalisée)

Exécutez la classe Utils une fois qu'elle est complétée et tester ses méthodes. Son exécution doit produire un affichage de la forme suivante :

11 7 12 0 8 13 16 4 1 2 14 5 3 6 9 10 17 15

Tester les classes pièces du jeu

Nous allons maintenant tester les classes précédentes. Dans le paquetage tests créez une nouvelle classe appelée TestPieces. La méthode main sera la seule méthode présente dans cette classe. Son exécution permettra les affichages/actions suivantes :

    Dans un premier temps, les paramètres du jeu seront saisis. Dans la suite nous supposerons que les paramètres par défaut ont été saisis par l'utilisateur.

    Une fois les paramètres saisis, le programme ouvrira la fenêtre de jeu avec une configuration initiale correspondant à un affichage par défaut sauf pour le message affiché qui sera "Cliquez pour continuer ...". Le programme attendra un clic de souris de l'utilisateur.

    Puis, le programme générera à l'aide de la méthode Piece.nouvellesPieces un tableau de 50 pièces. Les 49 premières pièces de ce tableau seront disposées sur le plateau dans l'ordre (la première pièce sera placée sur la première case de la première ligne, la deuxième pièce sera placée sur la deuxième case de la première ligne, ...). La dernière pièce du tableau (celle à l'indice 49) sera placée dans l'emplacement de la fenêtre correspondant à la pièce hors du plateau. Le programme affichera cette nouvelle configuration dans la fenêtre de jeu et attendra de nouveau un clic de l'utilisateur.

    Dans la suite de l'exécution du programme, l'utilisateur cliquera 4 fois. Après chaque clic, l'ensemble des pièces du tableau sont rotationnées puis de nouveau affichées. De plus, la représentation sous forme de chaîne de caractères de la pièce hors plateau est affichée dans la console après chaque clic. Le programme s'arrêtera après un cinquième clic de l'utilisateur.

    L'affichage dans la Console est similaire au suivant :

    [m:2|o:2|pe:false true true true]
    [m:2|o:3|pe:true false true true]
    [m:2|o:0|pe:true true false true]
    [m:2|o:1|pe:true true true false]
    
    Génération de la documentation HTML

Générez la documentation HTML des classes de votre projet Labyrinthe à l'aide de l'utilitaire Javadoc.

Cette documentation devra se trouver dans un dossier doc de votre projet.
IDE Eclipse

Après avoir cliqué sur votre projet Java, ouvrez l'entrée du menu Project

Generate Javadoc ...
IDE IntelliJ IDEA

Sélectionnez l'entrée du menu Tools

Generate Javadoc ...
Génération d'une archive

Générez une archive .zip correspondant à votre projet. L'archive devra porter comme nom labyrinthe_etape2.zip et devra contenir les sources de vos classes et leur documentation.
IDE Eclipse

Après avoir cliqué sur votre projet Java, utilisez l'entrée du menu contextuel Export ...
IDE IntelliJ IDEA

Après avoir cliqué sur votre projet Java, utilisez l'entrée du menu contextuel File ...
Travail à rendre

Sous Moodle, dans la section Projet Tutoré S2.

    Rappel : un seul étudiant par groupe effectue les dépôts du début à la fin du projet.

    Dans le dépôt Etape2 :
        Déposez le fichier labyrinthe_etape2.zip.
        Avant le mercredi 12 mai 2021 (23h).



